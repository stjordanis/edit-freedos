# FreeDOS Edit

FreeDOS improved clone of MS-DOS Edit


## Contributions

NLS specific corrections, updates and submissions should not be 
directly to submitted this project. NLS is maintained at the [FD-NLS](https://github.com/shidel/fd-nls)
project on GitHub. If the project is still actively maintained by it's
developer, it may be beneficial to also submit changes to them directly.

## EDIT.LSM

<table>
<tr><td>title</td><td>FreeDOS Edit</td></tr>
<tr><td>version</td><td>0.9b</td></tr>
<tr><td>entered&nbsp;date</td><td>2008-04-09</td></tr>
<tr><td>description</td><td>FreeDOS improved clone of MS-DOS Edit</td></tr>
<tr><td>keywords</td><td>Edit Editor</td></tr>
<tr><td>author</td><td>Shaun Raven &lt;shaun@cmlmicro.co.uk&gt;</td></tr>
<tr><td>maintained&nbsp;by</td><td>Aitor Santamaria-Merino &lt;aitor.sm (#) gmail.com&gt;</td></tr>
<tr><td>platforms</td><td>DOS (Borland C)</td></tr>
<tr><td>copying&nbsp;policy</td><td>[GNU General Public License, Version 2](LICENSE)</td></tr>
<tr><td>wiki&nbsp;site</td><td>http://wiki.freedos.org/wiki/index.php/Edit</td></tr>
</table>
